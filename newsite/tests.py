
import datetime
import time
from unittest import mock

import pytz

from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import index

class Story8Test(TestCase):
    def test_landing_page_exists(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)
    def test_invalid_url(self):
        response = Client().get('/status/hehe/')
        self.assertEqual(response.status_code, 404)
    
    def test_landing_page_uses_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'index.html')

# class Story8FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story8FunctionalTest, self).setUp()
     
#      def tearDown(self):
#         self.selenium.quit()
#         super(Story8FunctionalTest, self).tearDown()
        
