from django.core.validators import MaxLengthValidator
from django.db import models


class Status(models.Model):
    status = models.CharField(max_length=300,
                              validators=[MaxLengthValidator(300)])
    created_at = models.DateTimeField(auto_now_add=True)