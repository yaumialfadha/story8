from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse



def index(request):
    html = 'index.html'
    return render(request, html)

